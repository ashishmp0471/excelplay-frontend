import React, { useEffect } from 'react';
import '../../../App.scss';
import KryptosInfoBar from '../KryptosInfoBar/KryptosInfoBar';
import KryptosQuestion from '../KryptosQuestion/KryptosQuestion';
import KryptosHintModal from '../KryptosHintModal/KryptosHintModal';
import {
  NO_LEVELS_LEFT,
  MESSAGE_WHEN_ALL_LEVELS_COMPLETE,
  MESSAGE_WHEN_CORRECT_ANSWER,
  MESSAGE_WHEN_WRONG_ANSWER,
  //NO_HINTS,
} from '../../../components/common/Constants'
import {
  fetchQuestion,
  fetchRank,
  submitKryptosAnswer,
  getStaticAsset,
} from '../KryptosApi/ApiCalls';
import {useSelector, useDispatch} from 'react-redux';
import {rootType} from '../../../store/Reducers/rootReducer';

const KryptosPlay = () => {
  // const [level, setLevel] = useState(1);
  // const [rank, setRank] = useState(0);
  // const [imgUrl, setImgUrl] = useState('');
  // const [sourceHint, setSourceHint] = useState('');
  // const [hintText, setHintText] = useState([NO_HINTS]);

  const {level, rank, imgUrl, sourceHint, hintText} = useSelector((state: rootType) => state.Kryptos);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchQuestion().then(data => {
      if (!data.completed) {
        if (data.filetype !== 'NI') {
          //setImgUrl(getStaticAsset(data.level_file));
          dispatch({
            type: 'SET_IMG_URL',
            payload: getStaticAsset(data.level_file)
          });
        }
        //setLevel(data.level);
        dispatch({
          type: 'SET_LEVEL',
          payload: data.level
        });
        //setSourceHint(data.source_hint);
        dispatch({
          type: 'SET_SOURCE_HINT',
          payload: data.source_hint
        });
        if (data.hints.length) {
          const hints = data.hints.map((e: { hint: any; }) => {
            return e.hint;
          });
          //setHintText(hints);
          dispatch({
            type: 'SET_HINT_TEXT',
            payload: hints
          });
        }
      } else {
        window.alert(MESSAGE_WHEN_ALL_LEVELS_COMPLETE);
        //setLevel(parseInt(NO_LEVELS_LEFT));
        dispatch({
          type: 'SET_LEVEL',
          payload: parseInt(NO_LEVELS_LEFT)
        });
      }
    });

    fetchRank().then(data => {
      if (data.kryptos) {
        dispatch({
          type: 'SET_RANK',
          payload: data.kryptos.rank
        });
      } //setRank(data.kryptos.rank);
      else{
        dispatch({
          type: 'SET_RANK',
          payload: 1
        });
      } //setRank(1);
    });
  }, [dispatch]);

  const onSubmit = (ans: string) => {
    submitKryptosAnswer(ans).then(data => {
      if (data.answer === 'Correct') {
        window.alert(MESSAGE_WHEN_CORRECT_ANSWER);
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      } else {
        window.alert(MESSAGE_WHEN_WRONG_ANSWER);
      }
    });
  };

  return (
    <div>
      <KryptosInfoBar level={level} rank={rank} />
      <KryptosQuestion
        imgUrl={imgUrl}
        sourceHint={sourceHint}
        onSubmit={(ans: string) => onSubmit(ans)}
      />
      <KryptosHintModal hintText={hintText} />
    </div>
  );
};

export default KryptosPlay;
