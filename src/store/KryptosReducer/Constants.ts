
interface ListElm { 
  pic?: string;
  name?: string;
  rank?: string;
  level?: string;
};

export interface stateType {
  ranklist: ListElm[];
  level: number,
  rank: number,
  imgUrl: string,
  sourceHint: string,
  hintText: string[],
}