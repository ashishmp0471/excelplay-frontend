export const SEND_MESSAGE = 'SEND_MESSAGE'
export const DELETE_MESSAGE = 'DELETE_MESSAGE'

interface SendMessageAction {
  type: string;
  payload: String;
}

interface DeleteMessageAction {
  type: string;
  meta: {
    timestamp: number
  };
}

export type DalalbullActionType = SendMessageAction | DeleteMessageAction