import {DalalbullActionType} from "./Constants";

export interface initState {
  token: string,
  error?: string,
}

const initialState: initState = {
  token: '',
  error: '',
};

const DalalbullReducer = (state = initialState, action: DalalbullActionType) => {
  switch (action.type) {
    case 'AUTH_SUCCESS':
      return {
        ...state,
      };
    case 'AUTH_FAIL':
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default DalalbullReducer;